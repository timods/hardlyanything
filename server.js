const Gun = require('gun');
const path = require('path');
const express = require('express');
const app = express();

const port = (process.env.PORT || 8080);
app.use(Gun.serve);
const indexPath = path.join(__dirname, 'dist/index.html');
app.use(express.static('dist'));
app.get('*', function (_, res) {
  res.sendFile(indexPath);
});

const server = app.listen(port);
// TODO: change this to use either S3 or a custom filesystem adapter that I'd be building
Gun({ file: 'db/data.json', web: server });
