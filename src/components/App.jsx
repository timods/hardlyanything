import React, { Component, PureComponent} from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom'
import styled, { ThemeProvider } from 'styled-components'
import Gun from 'gun';
import {Flex, Box, Text, Link, Heading} from 'rebass'
import _ from 'lodash';
import { IoMdAdd } from "react-icons/io";

import theme from './theme'

const linkKeys = ["url", "tags", "interval",
                  "dateAdded", "title", "visits",
                  "dateDismissed"]

const msPerDay = 60000 * 60 * 24
const LinkList = styled.ul`
    list-style: none;
`

const VerticalList = (props) => (
        <Flex flexDirection={'row'} justifyContent={'center'}>
          <LinkList>
            {props.children}
          </LinkList>
        </Flex>)

const isActive = (link) => {
    let today = (new Date()).getTime()
    let next = (new Date(link["dateDismissed"]).getTime() + msPerDay * link["interval"])
    let active = today >= next
    return active;
}

const Header = (props) => (
    <Flex flexDirection={'row'} mt={1} className="header">
      <Link href={'/'}>
        <Heading fontSize={2} color={'black'}>{
            (new Date()).toLocaleDateString("en-us", {month: "long", day: "numeric"})}
        </Heading>
      </Link>
      <Link href={'/all'}>
        <Text ml={4} color={'grey'} fontSize={2}>View all</Text>
      </Link>
      <Link href={'/follow'} style={{marginLeft: 'auto'}} color={'grey'}>
      <IoMdAdd style={{verticalAlign: 'text-top'}}/> Follow
      </Link>
    </Flex>
);

class App extends Component {
    constructor() {
        super();
        this.gun = Gun(location.origin + '/gun');
        window.gun = this.gun;
        this.linkRef = this.gun.get("links")

        this.state = {links: []}
    }

    componentWillMount() {
        let links = this.state.links;
        const self = this;
        this.gun.get('links').on((n) => {
            var idList = _.reduce(n['_']['>'], function(result, value, key) {
                let data = { id: key, date: value};
                self.gun.get(key).on((linkData, key) => {
                    const merged = _.merge(data, _.pick(linkData, linkKeys));
                    const index = _.findIndex(links, (o)=>{ return o.id === key});
                    if(index >= 0) {
                        links[index] = merged;
                    }else{
                        links.push(merged);
                    }
                    self.setState({links});
                });
            }, []);
        });
    }


    onAddLink(data) {
        data["dateAdded"] = new Date()
        this.linkRef.set(this.gun.put(data))
    }

    updateDismissedDate(e, data) {
        let today = new Date();
        let link = _.pick(data, ["url", "tags", "interval",
                                 "dateAdded", "title", "visits"]);
        link["dateDismissed"] = (new Date).toString();
        this.gun.get(data.id).put(link);
    }

    renderLink(linkData) {
        return  (
            <li key={linkData.id}>
              <Link
                onClick={(e) => this.updateDismissedDate(e, linkData)}
                href={linkData["url"]}
                target={'_blank'}>
                <Text fontSize={7} mt={3} color={'black'}>
                  {linkData["title"]}
                </Text>
              </Link>
            </li>
        );
    }

    render() {
        return (
            <ThemeProvider theme={theme}>
              <Box p={3}>
                <Header/>
                <Router>
                  <div>
                    <Route exact path="/" render={(props) =>  (
                        <VerticalList>
                          {this.state.links.filter(isActive).map(this.renderLink.bind(this))}
                        </VerticalList>)}
                    />
                    <Route exact path="/all" render={(props) =>  (
                        <VerticalList>
                          {this.state.links.map(this.renderLink.bind(this))}
                        </VerticalList>)}
                    />
                  </div>
                </Router>
              </Box>
            </ThemeProvider>
        );
    }
}

export default App;
