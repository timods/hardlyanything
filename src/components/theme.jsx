const theme = {
    breakpoints: [
        '40em', '52em', '64em'
    ],
    fontSizes: [
        12, 14, 16, 24, 32, 48, 64, 96, 128
    ],
    space: [
        // margin and padding
        0, 4, 8, 16, 32, 64, 128, 256
    ],
    colors: {
        black: '#011627',
        blue: '#07c',
        red: '#e10',
        grey: '#2F404E'
    }
}

export default theme;
